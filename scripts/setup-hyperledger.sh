echo "Downloading latest hyperledger production release"
curl -sSL https://bit.ly/2ysbOFE | bash -s

echo "Setting PATH environment variable"
FABRIC_PATH=$(cd fabric-samples && pwd)
export PATH=${FABRIC_PATH}/bin:${FABRIC_PATH}:$PATH

echo "$PATH"