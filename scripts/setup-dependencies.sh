echo "Uninstalling old docker versions if any"
apt-get remove docker docker-engine docker.io containerd runc

echo "Updating package database"
apt-get -y update

echo "Installing packages to allow apt to use a repository over HTTPS"
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

echo "Adding Docker’s official GPG key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "Verify fingerprint"
apt-key fingerprint 0EBFCD88

echo "Adding Docker’s stable repository"
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

echo "Updating the apt package index"
apt-get update

echo "Installing the latest version of Docker Engine - Community and containerd"
apt-get install -y docker-ce docker-ce-cli containerd.io

echo "Verify that Docker Engine - Community is installed correctly by running the hello-world image."
docker run hello-world

echo "Downloading the current stable release of Docker Compose"
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

echo "Applying executable permissions to the binary"
chmod +x /usr/local/bin/docker-compose
usermod -a -G docker $USER

echo "Checking Docker Compose version"
docker-compose --version

echo "Installing Python"
apt-get install -y python

echo "Checking Python version"
python --version

echo "Installing Node.js & NPM"
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt-get install -y nodejs

echo "Checking Node.js & NPM version"
node -v
npm -v

echo "Kindly log out for the changes to take effect."