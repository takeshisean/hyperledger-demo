
echo "Setting PATH environment variable"
FABRIC_PATH=$(cd fabric-samples && pwd)
export PATH=${FABRIC_PATH}/bin:${FABRIC_PATH}:$PATH

echo "$PATH"